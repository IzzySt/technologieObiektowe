﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ValueObject.Tests
{
    [TestClass]
    public class MoneyTests
    {
        [TestMethod]
        public void CanSubstractMoney()
        {
            Assert.AreEqual(Money.Zero, new Money(500) - new Money(500));
            Assert.AreEqual(new Money(500), new Money(750) - new Money(250));
            Assert.AreEqual(new Money(-10), new Money(20) - new Money(30));
        }

        [TestMethod]
        public void CanAddMoney()
        {
            Assert.AreEqual(new Money(-10), new Money(-30) + new Money(20));
            Assert.AreEqual(new Money(6), new Money(2) + new Money(4));
            Assert.AreEqual(new Money(998), new Money(450) + new Money(548));
            Assert.AreEqual(new Money(1000), new Money(500) + new Money(500));
        }

        [TestMethod]
        public void CanCalculatePercentage()
        {
            Assert.AreEqual("0.03", new Money(100).Percentage(3).ToString());
            Assert.AreEqual("0.00", new Money(1).Percentage(1).ToString());
            Assert.AreEqual("26.40", new Money(8800).Percentage(30).ToString());
        }

        [TestMethod]
        public void ShouldProjectMoneyToInteger()
        {
            Assert.AreEqual(10, new Money(10).IntValue);
            Assert.AreEqual(0, new Money(0).IntValue);
            Assert.AreEqual(-10, new Money(-10).IntValue);
        }

        [TestMethod]
        public void CanCreateMoneyFromInteger()
        {
            Assert.AreEqual("10.00", new Money(1000).ToString());
            Assert.AreEqual("10.12", new Money(1012).ToString());
            Assert.AreEqual("-5.65", new Money(-565).ToString());
        }

        [TestMethod]
        public void ShouldMoneyBeEqualTo()
        {
            Assert.IsTrue(new Money(500) == new Money(500));
            Assert.IsFalse(new Money(500) == new Money(-500));
            Assert.IsFalse(null == Money.Zero);
        }

        [TestMethod]
        public void ShouldMoneyBeInequalTo()
        {
            Assert.IsTrue(new Money(500) != new Money(-500));
            Assert.IsFalse(new Money(500) != new Money(500));
            Assert.IsTrue(null != Money.Zero);
        }
    }
}
