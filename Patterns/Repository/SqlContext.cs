﻿using Microsoft.EntityFrameworkCore;

namespace Repository
{
    public class SqlContext : DbContext
    {
        public DbSet<Address> Addresses { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //...
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //..
        }
    }

}