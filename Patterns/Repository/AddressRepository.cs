﻿using System.Threading.Tasks;

namespace Repository
{

    public interface IAddressRepository
    {
        Task<Address> Save(Address address);
        Task<Address> Find(long? id);
    }

    public class AddressRepository : IAddressRepository
    {
        private readonly SqlContext _context;
        public async Task<Address> Find(long? id)
        {
            return await _context.Addresses.FindAsync(id);
        }

        public async Task<Address> Save(Address address)
        {
            _context.Addresses.Update(address);
           // await _context.SaveChangesAsync();
            return address;
        }
    }
}
