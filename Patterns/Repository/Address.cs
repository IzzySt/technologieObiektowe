﻿namespace Repository
{
    public class Address
    {
        public long? Id { get; private set; }

        public string Country { get; set; }

        public string City { get; set; }

        public string Street { get; set; }

        public string PostalCode { get; set; }
    }
}
