﻿using System;
using System.Globalization;

namespace ValueObject
{
    public class Money : IEquatable<Money>
    {
        public static readonly Money Zero = new(0);

        public int IntValue { get; }

        public Money(int value)
        {
            IntValue = value;
        }

        public static Money operator -(Money money, Money other)
        {
            return new Money(money.IntValue - other.IntValue);
        }

        public static Money operator +(Money money, Money other)
        {
            return new Money(money.IntValue + other.IntValue);
        }

        public static bool operator !=(Money left, Money right)
        {
            return !Equals(left, right);
        }

        public static bool operator ==(Money left, Money right)
        {
            return Equals(left, right);
        }

        public bool Equals(Money other)
        {
            if (other == null) return false;
            if(this == other) return true;
            return this.IntValue == other.IntValue;
        }
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(this, obj)) return true;
            if (ReferenceEquals(obj, null)) return false;

            return Equals((Money)obj);
        }

        public Money Percentage(int percentage)
        {
            return new Money((int)Math.Round(percentage * this.IntValue / 100.0));
        }

        public override string ToString()
        {
            var value = (double)IntValue / 100;
            return value.ToString("0.00", CultureInfo.CreateSpecificCulture("en-US"));
        }

        public override int GetHashCode()
        {
            return (int)IntValue;
        }
    }
}
