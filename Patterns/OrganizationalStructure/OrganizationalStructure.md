# Struktura organizacji

- Treść:
  Zamodeluj strukturę organizacyjną w firmie. Przełożony, ma pod sobą pracowników, każdy pracownik może być przełożonym kolejnych pracowników, a może być zwykłym pracownikiem. Każdy pracownik ma możliwość pokazać poziom swojej szczęśliwości (poziom ten jest określany podczas tworzenia obiektu pracownika).
  Pytając o poziom szczęśliwości danego pracownika, wyświetl również informacje o poziome szczęśliwości wszystkich podwładnych pracowników.
