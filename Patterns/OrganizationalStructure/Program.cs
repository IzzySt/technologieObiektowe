﻿using System;

namespace OrganizationalStructure
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var managerIt = new Worker("Zbyszek", 5);
            var managerFinance = new Worker("Karolina", 8);
            var boss = new Worker("Elena", 1);
            var worker = new Worker("Janek", 7);
            var worker2 = new Worker("Janina", 6);

            IComponent mainStructure = new StructureComposite();
            mainStructure.Add(boss);

            IComponent branchStructure = new StructureComposite();
            branchStructure.Add(managerIt);
            branchStructure.Add(managerFinance);
            mainStructure.Add(branchStructure);

            IComponent subordinatesStructure = new StructureComposite();
            subordinatesStructure.Add(worker);
            subordinatesStructure.Add(worker2);            
            branchStructure.Add(subordinatesStructure);

            mainStructure.GetLevelOfHappyness();
        }
    }
}
