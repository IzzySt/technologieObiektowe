﻿namespace OrganizationalStructure
{
    public interface IComponent
    {
        void GetLevelOfHappyness();

        void Add(IComponent worker);

        void Remove(IComponent worker);
    }
}
