﻿using System;
using System.Collections.Generic;

namespace OrganizationalStructure
{
    public class StructureComposite : IComponent
    {
        protected IList<IComponent> structures = new List<IComponent>();

        public void Add(IComponent structure)
        {
            this.structures.Add(structure);
        }
        public void Remove(IComponent structure)
        {
            this.structures.Remove(structure);
        }

        public void GetLevelOfHappyness()
        {
            foreach(var worker in this.structures)
            {
                worker.GetLevelOfHappyness();
            }
        }
    }
}
