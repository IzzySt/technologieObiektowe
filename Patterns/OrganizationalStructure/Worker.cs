﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrganizationalStructure
{

    public class Worker : IComponent
    {
        private int levelOfHappyness { get; set; }
        private string name { get; set; }     

        public Worker(string name, int levelOfHappyness)
        {
            this.levelOfHappyness = levelOfHappyness;
            this.name = name;
        }

        public void GetLevelOfHappyness()
        {
            Console.WriteLine(this.name + " is happy at level: " + this.levelOfHappyness);
        }
        public void Add(IComponent worker)
        {
            Console.WriteLine("Cannot add subworker");
        }

        public void Remove(IComponent worker)
        {
            Console.WriteLine("Cannot remove subworker");
        }
    }
}
